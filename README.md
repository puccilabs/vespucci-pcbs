This repository contains KiCAD PCB schematic and layout design files for the Vespucci infectious disease surveillance system designed by Puccinelli Laboratories, Inc. Individual module variants are grouped by module type in the root directory. The root also includes a directory titled "Vespucci" where intermodule connections are outlined symbolically. That directory does not have functional relevance for PCB fabrication, but it can help with schematic design of new modules and the recycling of previously used components to minimize the number of unique components used in all modules.

All hardware associated with this project adheres to "CERN Open Hardware Licence Version 2 - Strongly Reciprocal" open source hardwre license and may be freely distributed and modified as described.

